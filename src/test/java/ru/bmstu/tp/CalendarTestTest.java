package ru.bmstu.tp;

import org.joda.time.LocalDate;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.testng.Assert.*;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import java.io.File;
import java.util.Arrays;

public class CalendarTestTest {

    private static final Logger LOG = LoggerFactory.getLogger(CalendarTestTest.class);
    private WebDriver webDriver;
    private CalendarTestPage page;

    @Parameters({"browser", "url"})
    @BeforeClass
    public void SetupBrowser(String browser, String url) {
        LOG.debug("Using {} browser", browser);
        WebDriver driver;
        if (browser.equals("chrome")) {
            File file = new File("drivers/chromedriver.exe");
            System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());
            driver = new ChromeDriver();
        } else {
            driver = new FirefoxDriver();
        }
        driver.get(url);
        webDriver = driver;
        page = new CalendarTestPage(driver);
    }

    @Test
    public void testCurrentYear() {
    	int curYear = page.getCurrentYear();
    	assertEquals(curYear, LocalDate.now().getYear());
    }
    
    // При запросе "календарь февраль" будет отрисован календарь за текущий год, 
    // и месяц февраль окажется среди первых трех месяцев       
    @Test
    public void testMonth() throws InterruptedException {
    	String query = "rfktylfhm atdhfkm"; // "календарь февраль" в английской раскладке
    	page.search(query);
    	Thread.sleep(1000);
    	int curYear = page.getCurrentYear();
    	assertEquals(curYear, LocalDate.now().getYear());
    	String[] ms = page.getFirstThreeMonths();
    	assertTrue(Arrays.asList(ms).contains("Февраль"));
    }

    @Test
    public void testCurrentDay() {
    	int day = page.getCurrentDay();
    	assertEquals(day,LocalDate.now().getDayOfMonth());
    }
    
    @Test
    public void testCurrentMonth() {
    	String month = page.getCurrentMonth();
    	assertEquals(month,"Ноябрь");
    }

    @Test
    public void testLeapYear() throws InterruptedException {
        page.setYear(2012);
    	Thread.sleep(1000);
        int daysInFeb = page.getNumberOfDaysInFebruary();
        assertEquals(daysInFeb,29);
    }
    
    @Test
    public void testHolidays() throws InterruptedException {
    	for(int year = 2010; year <= 2014; ++year) {
    		page.setYear(year);
    		Thread.sleep(5000);
    		assertTrue(page.isHoliday("Март", 8) || page.isWeekend("Март", 8));
    		assertTrue(page.isHoliday("Январь", 1) || page.isWeekend("Январь", 1));
    		assertTrue(page.isHoliday("Май", 9) || page.isWeekend("Май", 9));
    	}    	
    }

    @AfterClass
    public void CloseDriver() {
        webDriver.quit();
    }
}
