package ru.bmstu.tp;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;


public class CalendarTestPage {
    private WebDriver webDriver;

    private final By yearLocator = By.xpath("//*[@id=\"yearsContainer\"]");
    private final By curYearLocator = By.xpath("//*[@id=\"yearsContainer\"]/span");
    private final By inputFormLocator = By.xpath("//*[@id=\"q\"]");
    private final By buttonLocator = By.xpath("//*[@id=\"MSearch\"]/div[2]/button");
    private final By curDayLocator = By.cssSelector(".calendar__currentday div");
    private final By curMonthLocator = By.xpath("//*[@id=\"calContent\"]/table/tbody/tr[4]/td/table[tbody[tr[td[contains(@class,'calendar__currentday')]]]]/caption");
    private final By lastFebDayLocator = By.xpath("//*[@id=\"calContent\"]/table/tbody/tr[1]/td[2]/table/tbody/tr[6]/td[3]/div");

    public CalendarTestPage(WebDriver webDriver) {
        this.webDriver = webDriver;
    }
    
    public int getCurrentYear() {
    	String yearString = webDriver.findElement(curYearLocator).getText();
    	return Integer.parseInt(yearString);
    }
    
    public String[] getFirstThreeMonths() {
    	String[] months = new String[3];
    	for(int i = 0; i < 3; ++i)
    		months[i] = webDriver.findElement(By.xpath("//*[@id=\"calContent\"]/table/tbody/tr[1]/td["+(i+1)+"]/table/caption")).getText();
    	return months;
    }

    public void search(String query) {
      WebElement element = webDriver.findElement(inputFormLocator);
      element.clear();
      element.sendKeys(query);
      webDriver.findElement(buttonLocator).click();
    }
    
    public int getCurrentDay() {
    	String dayString = webDriver.findElement(curDayLocator).getText();
    	int day = Integer.parseInt(dayString);
    	return day;
    }
    
    public String getCurrentMonth() {
    	String month = webDriver.findElement(curMonthLocator).getText();
    	return month;
    }

    public void setYear(final int year) {
    	WebElement yearCell = webDriver.findElement(yearLocator);
    	yearCell.click();			
    	webDriver.findElement(By.xpath("//*[@id=\"yearsContainer\"]/span/ul/li[contains(.," + year + ")]")).click();
    }
 
    public int getNumberOfDaysInFebruary() {
    	String numString = webDriver.findElement(lastFebDayLocator).getText();
    	return Integer.parseInt(numString);
    }
    
    public String dayType(String month, int day) {
    	return webDriver.findElement(By.xpath("//*[@id=\"calContent\"]/table/tbody/tr[1]/td/table[contains(caption,'"
				+month+"')]/tbody/tr/td[div="+day+"]")).getAttribute("class");
    }
    
    public boolean isHoliday(String month, int day) {
    	String type = dayType(month,day);
    	if(type.contains("holyday"))
    		return true;
    	return false;
    }
    
    public boolean isWeekend(String month, int day) {
    	String type = dayType(month,day);
    	if(type.contains("weekend"))
    		return true;
    	return false;    	
    }
}

